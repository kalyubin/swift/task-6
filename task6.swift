import Foundation


func address(of object: UnsafeRawPointer) -> String {
    let addr = Int(bitPattern: object)
    return String(format: "%p", addr)
}

func address(off value: AnyObject) -> String {
    return "\(Unmanaged.passUnretained(value).toOpaque())"
}


struct Identifier {
    var id = 1
}



class Ref<T> {
    var value: T
    init( value: T) {
        self.value = value
    }
}

struct Container<T> {
    var ref: Ref<T>
    init( value: T) {
        self.ref = Ref(value: value)
    }

    var value: T {
        get {
            ref.value
        }
        set {
            guard (isKnownUniquelyReferenced(&ref)) else {
                ref = Ref(value: newValue)
                return 
            }
            ref.value = newValue
        }
    }
}

var id = Identifier()
var container1 = Container(value: id)
var container2 = container1


print("Copy before write:")
print(address(off: container1.ref))
print(address(off: container2.ref))

container2.value.id = 21

print("After write:")
print(address(off: container1.ref))
print(address(off: container2.ref), "- address changed")

print()

/*------------------------------------------------------------------*/

protocol Hotel {
    init(roomNumber: Int)
}

class MyHotel: Hotel {
    var roomNumber: Int
    
    required init(roomNumber: Int) {
        self.roomNumber = roomNumber
    }
}

let myHotel = MyHotel(roomNumber: 50)
print("MyHotel number of rooms: \(myHotel.roomNumber)")

print()

/*------------------------------------------------------------------*/

protocol DiceGame {
    var diceNumber: Int { get }
}

extension Int: DiceGame {
    var diceNumber: Int {
        print("\(self) on the diсe.")
        return self
    }
}

let diceCoub = 4
let result = diceCoub.diceNumber

print()

/*------------------------------------------------------------------*/

protocol ArrivalProtocol {
    var planeName: String { get }
    var flightDescription: String? { get }
    
    func planeArrived()
}


extension ArrivalProtocol {
    var flightDescription: String? {
        return nil
    }
}

class Arrival: ArrivalProtocol {
    var planeName: String
    
    init(name: String) {
        self.planeName = name
    }
    

    
    func planeArrived() {
        print("The \(planeName) plane has arrived")
    }
}

let arrival = Arrival(name: "Aeroflot")
arrival.planeArrived()

print()

/*------------------------------------------------------------------*/

protocol Coding {
    var time: Int { get set }
    var linesOfCode: Int { get set }
    func writeCode(platform: Platform, numberOfSpecialists: Int)
}

protocol Testing {
    func stopCoding()
}

enum Platform {
    case iOS
    case Android
    case Web
}

class Company: Coding, Testing {
    var numberOfProgrammers: Int
    var specializations: [String]

    init(numberOfProgrammers: Int, specializations: [String]) {
        self.numberOfProgrammers = numberOfProgrammers
        self.specializations = specializations
    }

    var time: Int = 0
    var linesOfCode: Int = 0

    func writeCode(platform: Platform, numberOfSpecialists: Int) {
        print("Development has begun. Writing code for \(platform)")


        self.time += 1 
        self.linesOfCode += 100 

        print("Work completed. Submitting to testing")
    }

    func stopCoding() {
        if self.time > 5 {
            print("Exceed development time")
        } else {
            print("Stopping development has been successfully completed.")
        }
    }
}

let company = Company(numberOfProgrammers: 10, specializations: ["iOS", "Android", "Web"])
company.writeCode(platform: .iOS, numberOfSpecialists: 5)
company.stopCoding()